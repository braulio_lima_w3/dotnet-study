using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace csfeatures
{
    static class BasicFeatures
    {
        public static void Run()
        {
            Console.WriteLine("\n1. Dyanmics:");
            PlayingWithDynamics();

            Console.WriteLine("\n2. Delegates");
            Delegates();

            Console.WriteLine("\n3. Inheritance");
            Inheritance();

            Console.WriteLine("\n4. Yield");
            LuckyNumber();

            Console.WriteLine("\n5. Using");
            Using();

            Console.WriteLine("\n6. Attributes + reflection");
            Attributes();
        }
        public static void PlayingWithDynamics()
        {
            dynamic x = 10;
            x = "please hello me ";
            Console.WriteLine(x + x);
            x = new ExpandoObject();
            x.A = 100;
            x.B = 200;
            x.Add = new Func<int>( () => x.A + x.B );
            Console.WriteLine(x.Add());
        }

        delegate void DealNum(int val);
        static event DealNum DealNumEvent;
        public static void Delegates()
        {
            DealNum print = new DealNum(new Action<int>( x =>{ Console.WriteLine(x * x); }));
            print(2);
            DealNumEvent += new DealNum(new Action<int>( x => { for(var i = 0; i < x; i++) Console.WriteLine(i); } ));
            DealNumEvent += print;
            DealNumEvent(5);
        }

        public static void Inheritance()
        {
            // c# doesn't support inline instancing
            Scream scream = new Scream("MAXIMILLIAN PEGASUS!!!");
            YayScream yayScream = new YayScream("Hek pops");
            IYay iyay = yayScream;
            iyay.Yaayyyy();
            scream.Ahhhh();
            yayScream.Ahhhh();
        }

        private static IEnumerable<int> LuckyNumbers(int limit)
        {
            Random random = new Random();
            for (int i = 0; i < limit; i++)
            {
                if(random.Next() % 2 == 0){
                    yield return i;
                }
            }
        }

        public static void LuckyNumber()
        {
            StringBuilder builder = new StringBuilder();
            
            foreach(var val in LuckyNumbers(15))
            {
                if(builder.Length != 0)
                {
                    builder.Append(", ");
                }
                builder.Append(val);
            }

            Console.WriteLine(builder.ToString());
        }

        public static void Using()
        {
            using(var food = new DisposableFood("Waffles with maple syrup"))
            {
                food.Eat();
            }

            using(var food = new DisposableFood())
            {
                food.Eat();
            }

            using(var food = new DisposableFood())
            {
                food.Food = "Pepperoni pizza";
                food.Eat();
            }
        }

        public static void Attributes(){
            AttributesTesting.PrintUse();
            AttributesTesting.UsePls = "Minecraft is better than Fornite. Fite me. (७ò_ó)७";
            AttributesTesting.PrintUse();
            Console.WriteLine(AttributesTesting.GetInternalUse());

            // following will warn
            // AttributesTesting.NoUse = " ayy";
            // Console.WriteLine(AttributesTesting.NoUse);
            
            // following will err
            // Console.WriteLine(AttributesTesting.GetUse());

            // using reflection to obtain attributes`
            AttributesTesting.AttributeReflection();
        }

    }

    interface IYay
    {
        void Yaayyyy();
        // string What { get; set; }
    }
    class Scream
    {
        public string What { get;set; }
        public Scream(string what){
            What = what;
        }
        public virtual void Ahhhh(){
            Console.WriteLine(What);
        }
    }
    class YayScream : Scream, IYay
    {
        public YayScream(string what) : base(what) { }
        public void Yaayyyy()
        {
            Console.WriteLine($"Yayyyyyayyyy. YEAH! {What}");
        }

        public override void Ahhhh(){
            Console.WriteLine("Wha.. Wahhh...");
            base.Ahhhh();
            this.Yaayyyy();
        }
    }

    class DisposableFood : IDisposable
    {
        public string Food { get; set; } 
        public DisposableFood() { }
        public DisposableFood(string food) : this()
        {
            Food = food;
        }
        public void Dispose()
        {
            if(!String.IsNullOrEmpty(Food))
            {
                Console.WriteLine($"{Food} (and/or its leftovers) is in the trash!");
            }
            else
            {
                Console.WriteLine("No food to throw away, sorry.");
            }
        }

        public void Eat()
        {
            if(!String.IsNullOrEmpty(Food))
            {
                Console.WriteLine($"*munch munch*{Food}*munch munch*");
            }
            else
            {
                Console.WriteLine("Nothing to eat");
            }
        }
    }

    class AttributesTesting
    {
        [Obsolete("No use this spakek.")]
        public static string NoUse { get; set; }

        [Obsolete("I said no use the SPAKEK!", true)]
        public static string GetUse(){
            return "spakek";
        }

        private static string use = "pls use";
        public static string UsePls
        {
            get {
                return use;
            }
            set {
                use = value;
            }
        }
        public static void PrintUse()
        {
            Console.WriteLine(UsePls);
        }
        public static string GetInternalUse()
        {
            return use;
        }

        public static void AttributeReflection()
        {
            object[] butes = typeof(Person).GetCustomAttributes(true);

            PrintAttributes(butes);

            PropertyInfo[] info = typeof(Person).GetProperties();
            foreach (var item in info)
            {
                Console.WriteLine($"- {item.Name} is a function....");
                if("Name" == item.Name)
                {
                    PrintAttributes(item.GetCustomAttributes(true));
                }
            }
        }

        public static void PrintAttributes(object[] butes)
        {
            if(butes == null || butes.Length == 0)
            {
                return;
            }

            foreach (var obj in butes)
            {
                if( !(obj is MyAttribute ) )
                {
                    break;
                }
                MyAttribute bute = (MyAttribute) obj;
                Console.WriteLine($"{bute.Name} and {bute.Value}");
            }
        }


    }

    abstract class MyAttributes : Attribute
    {
        public string Value { get; set; }
    }

    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Property | System.AttributeTargets.Method, AllowMultiple = true)]
    class MyAttribute : MyAttributes
    {
        public string Name { get; set; }
    }

    [MyAttribute(Name = "Human", Value = "Bean")]
    public class Person
    {
        [MyAttribute(Name = "Field", Value = "Onamae wa")]
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }

}