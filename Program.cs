﻿using System;

namespace csfeatures
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Write("Play with database? y/n: ");
            if(Console.ReadKey().KeyChar == 'y')
            {
                DatabaseInterface.SemiMain(args);
            }
            else
            {
                // basic features chapter 1
                Console.WriteLine("Defaulting to simple testing.\nBasic features\n");
                BasicFeatures.Run();

                Console.WriteLine("\n\n-----------------------------");
                Console.WriteLine("\nReading from a file");

                // simple reading of a file...
                ReadingFiles.ReadingThemFiles();
            }

            
        }
    }
}
