using System;
using System.Collections.Generic;
using csfeatures.Context;
using csfeatures.Models;
using System.Linq;
using System.Text;

namespace csfeatures
{

    public static class DatabaseInterface
    {
        public static void SemiMain(string[] args)
        {
            DataInterface.Runner();
        }
    }

    static class DatabaseInstance
    {
        private static MovieContext context;

        public static MovieContext GetContext()
        {
            if(context == null)
            {
                context = new MovieContext();
                context.Database.EnsureCreated();
            }
            return context;
        }

        public static List<Movie> AllMovies()
        {
            var ctx = GetContext();
            return ctx.Movies.ToList();
        }

        public static Movie FindOne(string name="", int id=0)
        {
            if(String.IsNullOrEmpty(name) && id <= 0)
            {
                return null;
            }
            var ctx = GetContext();
            if(id > 0)
            {
                return ctx.Movies.Find(id);
            }
            var found = ctx.Movies.Where(i => name == i.Name);
            if(found.Count() > 0){
                return found.First();
            }
            return null;
        }

        public static Movie SaveMovie(Movie movie)
        {
            if(movie == null)
            {
                return movie;
            }
            validate(movie);

            var ctx = GetContext();
            var found = FindOne(movie.Name);
            if(found != null)
            {
                movie = found;
                ctx.Movies.Update(movie);
            }
            else
            {
                ctx.Add(movie);
            }
            ctx.SaveChanges();
            return movie;
        }

        public static bool DeleteMovie(int id=0, string name="", Movie movie=null)
        {
            var ctx = GetContext();
            if(movie == null)
            {
                var found = FindOne(id:id, name:name);
                if(found == null)
                {
                    System.Console.WriteLine("Cannot delete an empty value");
                    return false;
                }
                movie = found;
            }
            ctx.Movies.Remove(movie);
            ctx.SaveChanges();
            return true;
        }

        private static void validate(Movie movie)
        {
            if(movie == null)
            {
                return;
            }

            if(String.IsNullOrWhiteSpace(movie.Name))
            {
                throw new Exception("Name is missing");
            }
            if(movie.IMDB != null && !(movie.IMDB >= 0 && movie.IMDB <= 10))
            {
                throw new Exception("Range for IMDB rating is invalid. from 0 to 10 (inclusive)");
            }
        }
    }

    static class DataInterface
    {
        static Movie selected = null;

        public static void Runner()
        {
            while (true)
            {
                bool kill = false;

                Console.WriteLine();
                
                if(selected != null)
                {
                    Console.WriteLine($"Movie \"{selected.Name}\" is selected.");
                }
                Console.WriteLine("Choose an option:");
                Console.WriteLine($"[a] to {(selected == null ? "add" : "edit")} a movie");
                Console.WriteLine("[l] to list the movies");
                Console.WriteLine("[f] to select a movie");
                if(selected != null)
                {
                    Console.WriteLine("[i] to see movie's information");
                    Console.WriteLine("[c] to deselect a movie");
                    Console.WriteLine("[d] to delete a movie");
                }
                Console.WriteLine("[q] to quit");

                char option = Console.ReadKey().KeyChar;
                Console.WriteLine();

                if(!("alifq" + (selected != null? "cd" : "")).Contains(option))
                {
                    Console.WriteLine("Invalid option.\n\n");
                    continue;
                }

                switch(option)
                {
                    case 'a':
                        AddMovie();
                        break;
                    case 'l':
                        ListMovies();
                        break;
                    case 'i':
                        Details();
                        break;
                    case 'f':
                        SelectMovie();
                        break;
                    case 'c':
                        selected = null;
                        break;
                    case 'd':
                        DeleteMovie();
                        break;
                    case 'q':
                        kill = true;
                        break;
                }


                if(kill)
                {
                    return;
                }
            }
        }

        static string RequiredField(string field="Field")
        {
            string output = null;
            bool first = false;
            while(String.IsNullOrWhiteSpace(output))
            {
                if(first)
                {
                    Console.Write($"{field} must not be empty. Try again: ");
                }
                output = Console.ReadLine();
                first = true;
            }
            return output;
        }
        static void AddMovie()
        {
            if(selected == null)
            {
                Console.WriteLine($"Add a movie:\n\n");
            }
            else
            {
                Console.WriteLine($"Editing movie {selected.Name}:");
                Console.WriteLine("Leave fields empty if nothing changes.\n\n");
            }
            Movie m = selected == null ? new Movie() : selected;
            try
            {

                if(selected == null)
                {
                    Console.Write("Name: ");
                    m.Name = RequiredField("Name");

                    Console.Write("IMDB: ");
                    m.IMDB = double.Parse(Console.ReadLine());
                    
                    Console.Write("Digial: ");
                    m.Digial = bool.Parse(Console.ReadLine());

                    Console.Write("Synopsis: ");
                    m.Description = Console.ReadLine();
                }
                else
                {
                    string val = null;
                    Console.Write("IMDB: ");
                    val = Console.ReadLine();
                    if(!String.IsNullOrEmpty(val))
                    {
                        m.IMDB = double.Parse(val);
                    }
                    
                    Console.Write("Digial: ");
                    val = Console.ReadLine();
                    if(!String.IsNullOrEmpty(val))
                    {
                        m.Digial = bool.Parse(val);
                    }

                    Console.Write("Synopsis: ");
                    val = Console.ReadLine();
                    if(!String.IsNullOrEmpty(val))
                    {
                        m.Description = val;
                    }
                }

                DatabaseInstance.SaveMovie(m);

                Console.WriteLine("\nMovie saved.");

                Pause();
            }
            catch(Exception e)
            {
                Console.WriteLine("Could not make a movie. Reason: " + e.Message);
                
            }
        }
        

        const string ListFormat = "{0, 5} | {1, 20} | {2, 4} | {3, 7} | {4, 60}";      
        static string Accumilator(int limit=0)
        {
            StringBuilder b = new StringBuilder();
            while(limit-- > 0)
            {
                b.Append('-');
            }
            return b.ToString();
        }
        static string Trunc(string input, int length=60)
        {
            if(String.IsNullOrWhiteSpace(input))
            {
                return "";
            }
            else
            {
                input = input.Trim();
            }
            
            if(input.Length < length)
            {
                return input;
            }
            int maxWithDot = length - 3;
            return input.Substring(0, maxWithDot) + "...";
        }
        static void ListMovies()
        {
            Console.WriteLine(String.Format(ListFormat, "id", "name", "imdb", "digital", "synopsis"));
            Console.WriteLine(String.Format(ListFormat, Accumilator(4), Accumilator(20), Accumilator(4), Accumilator(7), Accumilator(60)));
            DatabaseInstance.AllMovies().ForEach(movie => {
                Console.WriteLine(String.Format(ListFormat, movie.MovieId, Trunc(movie.Name, 20), movie.IMDB, movie.Digial, Trunc(movie.Description, 60)));
            });
            Pause();
        }

        static void DeleteMovie()
        {
            if(selected == null)
            {
                Console.WriteLine("Movie not selected.");
                return;
            }
            DatabaseInstance.DeleteMovie(movie:selected);
            Console.WriteLine("Movie deleted.");
            selected = null;
        }

        static void SelectMovie()
        {
            bool quit = true;
            selected = null;

            while(true)
            {
                Console.WriteLine();
                Console.WriteLine("Select a movie by ID or Name?");
                Console.WriteLine("[i] for id");
                Console.WriteLine("[n] for name");
                Console.WriteLine("[c] to cancel");

                char option = Console.ReadKey().KeyChar;
                System.Console.WriteLine();
                if(!"inq".Contains(option))
                {
                    continue;
                }

                switch(option)
                {
                    case 'i':
                        Console.Write("ID: ");
                        try{
                            selected = DatabaseInstance.FindOne(id:int.Parse(Console.ReadLine()));
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("Error while finding. Reason: " + e.Message);
                        }
                        return;
                    case 'c':
                        Console.Write("Name: ");
                        selected = DatabaseInstance.FindOne(name:Console.ReadLine());
                        return;
                    default:
                        quit = true;
                        break;
                }

                if(selected == null)
                {
                    Console.WriteLine("Movie not found. Try again? (y/n) ");
                    quit = !(Console.ReadKey().KeyChar == 'y');
                }


                if(quit)
                {
                    return;
                }
            }
        }

        public static void Details()
        {
            if(selected == null)
            {
                Console.WriteLine("No movie selected.");
                return;
            }
            Console.WriteLine("\nMovie Information:");
            Console.WriteLine(" - Id: " + selected.MovieId);
            Console.WriteLine(" - Name: " + selected.Name);
            Console.WriteLine($" - IMDB: {selected.IMDB}/10");
            Console.WriteLine(" - Is Digital: " + selected.Digial);
            Console.WriteLine(" - Synopsis: " + selected.Description);
            
            Pause();
        }

        public static void Pause()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
            Console.WriteLine();
        }


    }

}