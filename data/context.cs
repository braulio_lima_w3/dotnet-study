using System;
using System.ComponentModel.DataAnnotations.Schema;
using csfeatures.Models;
using Microsoft.EntityFrameworkCore;

namespace csfeatures.Context
{
    public class MovieContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=MyLovelyDatabase.sqlite");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                .HasKey(movie => movie.MovieId);
            modelBuilder.Entity<Movie>()
                .Property(movie => movie.Name).IsRequired();
            modelBuilder.Entity<Movie>()
                .Property(movie => movie.MovieId)
                    .ValueGeneratedOnAdd();
        }
    }
}