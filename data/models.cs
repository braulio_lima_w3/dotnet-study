using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace csfeatures.Models
{
    [Table("movies")]
    public class Movie
    {
        // [Key]
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MovieId { get; set; }

        // [Required]
        public string Name { get; set; }
        public double? IMDB { get; set; }
        public string Description { get; set; }
        public DateTime? Release { get; set; }
        public bool? Digial { get; set; }
    }
}