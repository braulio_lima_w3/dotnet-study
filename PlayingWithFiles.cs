using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace csfeatures
{

    public static class ReadingFiles
    {
        const string SimpleTextFile = @"files/simple-txt.md";
        const string AJsonFile = @"files/a.json";
        const string UseTelephoneRegexp = @"((1[- ]?)?(\(\d{3}\)|(\d{3})))[- ]?\d{3}[- ]?\d{4}";

        public static void ReadingThemFiles()
        {
            Console.WriteLine("\n1. Reading telephones from file");
            ListPhones();

            Console.WriteLine("\n2. JSON to dict");
            JsonToDict();
        }

        static String ReadFile(string name)
        {
            return File.ReadAllText(name);
        }

        static void ListPhones()
        {
            string file = ReadFile(SimpleTextFile);
            MatchCollection matches = Regex.Matches(file, UseTelephoneRegexp);
            if(matches == null || matches.Count == 0)
            {
                return;
            }
            foreach(var m in matches)
            {
                Console.WriteLine(m);
            }
        }

        static void JsonToDict()
        {
            string jsonfile = ReadFile(AJsonFile);
            Dictionary<string, string> dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonfile);
            if(dictionary != null)
            {
                foreach(var key in dictionary.Keys)
                {
                    Console.WriteLine($"{key} is {dictionary.GetValueOrDefault(key, "")}");
                }
            }
            // had to execute: dotnet add package Newtonsoft.Json
        }

    }

}